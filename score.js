function validateForm()
{
	var score = 0;
	var ques1 = document.forms["myForm"]["ques1"].value;
	var ques2 = document.forms["myForm"]["ques2"].value;
	var ques3 = document.forms["myForm"]["ques3"].value;
	var ques4 = document.forms["myForm"]["ques4"].value;
	var ques5 = document.forms["myForm"]["ques5"].value;
	var ques6 = document.forms["myForm"]["ques6"].value;
	var ques7 = document.forms["myForm"]["ques7"].value;
	var ques8 = document.forms["myForm"]["ques8"].value;
	var ques9 = document.forms["myForm"]["ques9"].value;
	var ques10 = document.forms["myForm"]["ques10"].value;
	var name = document.forms["myForm"]["name"].value;


	if( ques1 == "")
	{
		alert("Oops!! Question 1 is required");
		return false;
	}
	if( ques2 == "")
	{
		alert("Oops!! Question 2 is required");
		return false;
	}
	if( ques3 == "")
	{
		alert("Oops!! Question 3 is required");
		return false;
	}
	if( ques4 == "")
	{
		alert("Oops!! Question 4 is required");
		return false;
	}
	if( ques5 == "")
	{
		alert("Oops!! Question 5 is required");
		return false;
	}
	if( ques6 == "")
	{
		alert("Oops!! Question 6 is required");
		return false;
	}
	if( ques7 == "")
	{
		alert("Oops!! Question 7 is required");
		return false;
	}
	if( ques8 == "")
	{
		alert("Oops!! Question 8 is required");
		return false;
	}
	if( ques9 == "")
	{
		alert("Oops!! Question 9 is required");
		return false;
	}
	if( ques10 == "")
	{
		alert("Oops!! Question 10 is required");
		return false;
	}



	if(document.getElementById('answer1').checked)
	{
		score++;
	}
	if(document.getElementById('answer2').checked)
	{
		score++;
	}
	if(document.getElementById('answer3').checked)
	{
		score++;
	}
	if(document.getElementById('answer4').checked)
	{
		score++;
	}
	if(document.getElementById('answer5').checked)
	{
		score++;
	}
	if(document.getElementById('answer6').checked)
	{
		score++;
	}
	if(document.getElementById('answer7').checked)
	{
		score++;
	}
	if(document.getElementById('answer8').checked)
	{
		score++;
	}
	if(document.getElementById('answer9').checked)
	{
		score++;
	}
	if(document.getElementById('answer10').checked)
	{
		score++;
	}



	if(score >= 0 && score <= 4)
	{
		alert("Keep trying," + name + "! You answered " + score + " out of 10 correctly");
	}

	if(score >= 5 && score <= 9)
	{
		alert("Way to go," + name + "! You got " + score + " out of 10 correct");
	}

	if(score == 10)
	{
		alert("Congratulations " + name + "! You got " + score + " out of 10");
	}
	
}